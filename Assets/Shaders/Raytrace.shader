﻿Shader "Unlit/Raytrace"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _G1 ("G1", Vector) = (0,0,0,0)
        _G2 ("G2", Vector) = (0,0,0,0)
        _A("A", Color) = (0,0,0)
        _D ("D", Color) = (0,0,0)
        _L("Light",Vector) = (0,0,0,0)
        _LA("LightAmbient",Vector) = (0,0,0,0)
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 100
        //Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
       

            #include "UnityCG.cginc"
            
#define MAX_STEPS 100
#define MAX_DIST 100
#define SURF_DIST 1e-3

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 ro : TEXCOORD1;
                float3 hitPos : TEXCOORD2;
            };

          
            float4 _G1;
            float4 _G2;
            float3 _A;
            float3 _D;
            fixed4 _L;
            fixed4 _LA;

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.ro =  mul(unity_WorldToObject,fixed4(_WorldSpaceCameraPos,1.0));
				o.hitPos =v.vertex;
                return o;
            }
            
            float sphere(in float3 p,float radius)
            {
                return length(p) - 0.5;
            }
            
            float sdCappedTorus(in float3 p, in float2 sc, in float ra, in float rb)
            {
                p.x = abs(p.x);
                float k = (sc.y*p.x>sc.x*p.y) ? dot(p.xy,sc) : length(p.xy);
                return sqrt( dot(p,p) + ra*ra - 2.0*ra*k ) - rb;
            }
            
            float sdCapsule( fixed3 p, fixed3 a, fixed3 b, float r )
            {
              fixed3 pa = p - a, ba = b - a;
              float h = clamp( dot(pa,ba)/dot(ba,ba), 0.0, 1.0 );
              return length( pa - ba*h ) - r;
            }


            float G(in float3 p,float radius)
            {
                
                float an = 2.5*(0.5+0.5*sin(_Time.y*1.1+3.14));
                an = 1.57*1.5;
                float2 c = float2(sin(an),cos(an));
                float3 cTorPos = p;
                float cTor = sdCappedTorus(cTorPos,c,radius*.8,0.025);
                float capsule = sdCapsule(p,fixed3(_G1.x,_G1.y,_G1.z),fixed3(_G2.x,_G2.y,_G2.z),0.025);
                float g = min(cTor,capsule);
               
                g = abs(g) - 0.1;
               
                float plane =  - 0.5;
                //dot(p,normalize(fixed3(0.,-1.,0.)))
                
                
                return max(plane,g);
                //return g;
            }
            
            float sdBox( fixed3 p, fixed3 b )
{
  fixed3 q = abs(p) - b;
  return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
}

fixed4x4 rotationAxisAngle( fixed3 v, float angle )
{
    float s = sin( angle );
    float c = cos( angle );
    float ic = 1.0 - c;

    return fixed4x4( v.x*v.x*ic + c,     v.y*v.x*ic - s*v.z, v.z*v.x*ic + s*v.y, 0.0,
                 v.x*v.y*ic + s*v.z, v.y*v.y*ic + c,     v.z*v.y*ic - s*v.x, 0.0,
                 v.x*v.z*ic - s*v.y, v.y*v.z*ic + s*v.x, v.z*v.z*ic + c,     0.0,
			     0.0,                0.0,                0.0,                1.0 );
}

fixed4x4 translate( float x, float y, float z )
{
    return fixed4x4( 1.0, 0.0, 0.0, 0.0,
				 0.0, 1.0, 0.0, 0.0,
				 0.0, 0.0, 1.0, 0.0,
				 x,   y,   z,   1.0 );
}


float4x4 inverse(float4x4 input)
 {
     #define minor(a,b,c) determinant(float3x3(input.a, input.b, input.c))
     //determinant(float3x3(input._22_23_23, input._32_33_34, input._42_43_44))
     
     float4x4 cofactors = float4x4(
          minor(_22_23_24, _32_33_34, _42_43_44), 
         -minor(_21_23_24, _31_33_34, _41_43_44),
          minor(_21_22_24, _31_32_34, _41_42_44),
         -minor(_21_22_23, _31_32_33, _41_42_43),
         
         -minor(_12_13_14, _32_33_34, _42_43_44),
          minor(_11_13_14, _31_33_34, _41_43_44),
         -minor(_11_12_14, _31_32_34, _41_42_44),
          minor(_11_12_13, _31_32_33, _41_42_43),
         
          minor(_12_13_14, _22_23_24, _42_43_44),
         -minor(_11_13_14, _21_23_24, _41_43_44),
          minor(_11_12_14, _21_22_24, _41_42_44),
         -minor(_11_12_13, _21_22_23, _41_42_43),
         
         -minor(_12_13_14, _22_23_24, _32_33_34),
          minor(_11_13_14, _21_23_24, _31_33_34),
         -minor(_11_12_14, _21_22_24, _31_32_34),
          minor(_11_12_13, _21_22_23, _31_32_33)
     );
     #undef minor
     return transpose(cofactors) / determinant(input);
 }

fixed4 iBox( in fixed3 ro, in fixed3 rd, in fixed4x4 txx, in fixed4x4 txi, in fixed3 rad ) 
{
    // convert from ray to box space

	fixed3 rdd = (mul(txx,fixed4(rd,0.0))).xyz;
	fixed3 roo = (mul(txx,fixed4(ro,1.0))).xyz;

	// ray-box intersection in box space
    fixed3 m = 1.0/rdd;
    fixed3 n = m*roo;
    fixed3 k = abs(m)*rad;
	
    fixed3 t1 = -n - k;
    fixed3 t2 = -n + k;

	float tN = max( max( t1.x, t1.y ), t1.z );
	float tF = min( min( t2.x, t2.y ), t2.z );
	
	if( tN > tF || tF < 0.0) return fixed4(-1,-1,-1,-1);

	fixed3 nor = -sign(rdd)*step(t1.yzx,t1.xyz)*step(t1.zxy,t1.xyz);

    // convert to ray space
	
	nor = (mul(txi,fixed4(nor,0.0))).xyz;

	return fixed4( tN, nor );
}

            float GetDist(float3 p)
            {
                //float d = sphere(p,0.5);
                //d = length(float2(length(p.xz) - 0.25,p.y)) - 0.1f;//torus
                float b = sdBox(p,fixed3(0.1,0.1,0.1));
                return b;
                //return G(p,0.5);
            }
            
           

            float4 Raymarch(float3 ro, float3 rd,fixed4x4 txx,fixed4x4 txi)
            {
                float dO = 0;
            	float dS;
                fixed3 n = (0,0,0);
            	for (int i = 0; i < MAX_STEPS; i++) {
            		float3 p = ro + rd * dO;
                    float4 iB = iBox(ro,rd,txx,txi,fixed3(0.5,0.5,0.5));
                    n = iB.yzw;
                    dS = iB.x;
            		dO += dS;
            		if (dS<SURF_DIST || dO>MAX_DIST) break;
            	}
            	return float4(dO,n);
            }
            
            float3 GetNormal(float3 p)
            {
                float2 e = float2(1e-2,0);
                
                float3 n = GetDist(p) - float3(
                    GetDist(p-e.xyy),
                    GetDist(p-e.yxy),
                    GetDist(p-e.yyx)
                );
                return normalize(n);
            }
            
            fixed4 frag (v2f i) : SV_Target
            {

                float2 uv = i.uv - .5;
				
				float3 ro =  i.ro;
				float3 rd =  normalize(i.hitPos - ro);

                // rotate and translate box	
	            //fixed4x4 rot = rotationAxisAngle( normalize(fixed3(1.0,1.0,0.0)), _Time.y );
	            //fixed4x4 tra = translate( 0.0, 0.0, 0.0 );

                fixed4x4 rot = rotationAxisAngle( normalize(fixed3(.0,.0,.0)), 0.0);
	            fixed4x4 tra = translate( 0.0, 0.0, 0.0 );



                
	            fixed4x4 txi = mul(tra,rot); //unity_WorldToObject;
	            fixed4x4 txx = inverse( txi );


				float4 col = 0;
                float4 dn = Raymarch(ro, rd,txi,txx);
				float d = dn.x;
                        

				if (d > MAX_DIST) {
					float3 p = ro + rd*d;

				//	col.rgb = GetNormal(p);

					col.rgb = dn.yzw;
					float dif = clamp( dot(col.rgb,_L.xyz), 0.0, 1.0 );
                    float amb = 0.5 + 0.5*dot(col.rgb,_LA.xyz);
                    col.rgb = _A*amb + _D*dif;
					 
				}
				else
                {
                    discard;
                    col.a = 0.5;
                }
				    
                
                
				return col;


          }
            
           

			

			
            ENDCG
        }
    }
}
