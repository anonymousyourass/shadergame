﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/Example"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _iMouse ("Mouse",Vector) = (0,0,0,0)
        _offset("off",Range(-10,10)) = 5.0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 100
        //Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
       

            #include "UnityCG.cginc"
            
// Rendering parameters
#define RAY_LENGTH_MAX		20.0
#define RAY_BOUNCE_MAX		10
#define RAY_STEP_MAX		80
#define LIGHT				float3 (1.0, 1.0, -1.0)
#define AMBIENT				0.2
#define SPECULAR_POWER		3.0
#define SPECULAR_INTENSITY	0.5
//
// Rendering options (for those who have a slow GPU)
//#define ICE_CUBE
#define PROPAGATION
#define DISPERSION

// Macros used to handle color channels
#ifdef DISPERSION
    
	#define CHANNEL(x) dot(x, channel)
#else
	//#define COLOR float3
	//#define CHANNEL(x) x
#endif

// Math constants
#define DELTA	0.001
#define PI		3.14159265359

float4 _iMouse;
 sampler2D _MainTex;
            float4 _MainTex_ST;
            float _offset;
            
// Smooth minimum
float smin (in float a, in float b, in float k) {
	float h = clamp (0.5 + 0.5 * (a - b) / k, 0.0, 1.0);
	return lerp (a, b, h) - k * h * (1.0 - h);
}

// Materials
struct Material {
	float3 color;
	float behavior; // from -1.0 (fully reflective) to 1.0 (fully transparent)
	float3 refractIndex; // not used if behavior < 0.0
};


Material material;
int materialTo;
int materialFrom = -1;

void getMaterial () {
	
	if (materialTo == 0) 
	{ // Glass
		material.color = float3 (0.8, 0.8, 1.0);
		material.behavior =_iMouse.z < 0.5 ? 0.9 : -0.9;
		material.refractIndex =float3 (1.50, 1.55, 1.60);
	} else if (materialTo == 1) 
	{ // Wine
		
		material.color = float3 (0.7, 0.0, 0.1);
        material.behavior =_iMouse.z < 0.5 ? 0.8 : 0.4;
        material.refractIndex =float3 (1.20, 1.21, 1.22);
		
	} else if (materialTo == 2) 
	{ // Table

		material.color = float3 (0.5, 0.5, 1.0);
        material.behavior =  _iMouse.z < 0.5 ? -0.5 : 0.5;
        material.refractIndex = float3 (2.0, 2.1, 2.2);
		
	} else 
	{ // materialIndex == 3 // Ice cube
		material.color = float3 (0.9, 0.9, 1.0);
        material.behavior =  0.9;
        material.refractIndex = float3 (1.31, 1.31, 1.31);
	}
	
}

// Distance to the glass
float getDistanceGlass (in float3 p, in float d, in float dxz) {
	d = max (max (d - 1.45, 1.4 - d), p.y - 0.5);
	d = min (d, max (dxz - 1.0, p.y + 3.0));
	d = smin (d, max (dxz - 0.2, p.y + 1.4), _iMouse.z < 0.5 ? 0.2 : 0.05);
	return max (d, -p.y - 3.05);
}

// Distance to the wine
float getDistanceWine (in float3 p, in float d) {
	return max (d - 1.4, p.y + p.x * 0.1 * cos (_Time.y * 2.0));
}

// Distance to the table
float getDistanceTable (in float3 p, in float dxz) {
	return max (max (dxz - 2.0, p.y + 3.05), -p.y - 4.0);
}

// Distance to the ice cube
float getDistanceIceCube (in float3 p) {
	float a = 0.2 * cos (_Time.y);
	p = float3 (p.x, mul(float2x2 (cos (a), sin (a), -sin(a), cos(a)) , p.yz));
	p += float3 (0.5, 0.25 - 0.1 * cos (_Time.y * 1.5), 0.0);
	const float r = 0.08;
	float3 o = abs (p) - 0.3 + r;
	float d = length (max (o, 0.0)) + min (max (o.x, max (o.y, o.z)), 0.0) - r;
	return d + 0.03 * cos (p.x * 9.0) * cos (p.y * 9.0 + 3.0) * cos (p.z * 9.0 + 6.0);
}

// Distance to a given material
float getDistanceMaterial (in float3 p, in int materialIndex) {
	float materialDist;
	if (materialIndex == 0) {
		materialDist = getDistanceGlass (p, length (p), length (p.xz));
	} else if (materialIndex == 1) {
		materialDist = getDistanceWine (p, length (p));
	} else if (materialIndex == 2) {
		materialDist = getDistanceTable (p, length (p.xz));
	} else { // materialIndex == 3
		materialDist = getDistanceIceCube (p);
	}
	return materialDist;
}
    
// Distance to the scene

float getDistanceScene (in float3 p) {
	float d = length (p);
	float dxz = length (p.xz);

	// Air
	materialTo = -1;
	float sceneDist = RAY_LENGTH_MAX;
    
	// Wine
	float materialDist = getDistanceWine (p, d);
	if (materialDist < 0.0)
	{
	    materialTo = 1; 	    
	}
	 sceneDist = min (sceneDist, materialFrom != 1 ? materialDist : -materialDist);//1

	// Ice cube
	#ifdef ICE_CUBE
	if (_iMouse.z < 0.5) {
		materialDist = getDistanceIceCube (p);
		if (materialDist < 0.0)
		{
		    materialTo = 3; 
		    
		} 
		sceneDist = min (sceneDist, materialFrom != 3 ? materialDist : -materialDist);    //3
	}
	#endif

	// Glass
	materialDist = getDistanceGlass (p, d, dxz);
	if (materialDist < 0.0)
	 {
	    materialTo = 0; 
	    
	 }
	 sceneDist = min (sceneDist, materialFrom != 0 ? materialDist : -materialDist);//0

	// Table
	materialDist = getDistanceTable (p, dxz);
	if (materialDist < 0.0)
	 {
	    materialTo = 2; 
	    
	 }
	 
    sceneDist = min (sceneDist, materialFrom != 2 ? materialDist : -materialDist);//2
	// Return the distance
	return sceneDist;
}

// Normal at a given point
float3 getNormal (in float3 p, in int materialIndex) {
	const float2 h = float2 (DELTA, -DELTA);
	return normalize (
		h.xxx * getDistanceMaterial (p + h.xxx, materialIndex) +
		h.xyy * getDistanceMaterial (p + h.xyy, materialIndex) +
		h.yxy * getDistanceMaterial (p + h.yxy, materialIndex) +
		h.yyx * getDistanceMaterial (p + h.yyx, materialIndex)
	);
}

// Cast a ray for a given color channel (and its corresponding refraction index)
static float3 lightDirection = normalize (LIGHT);

float raycast (in float3 origin, in float3 direction, in float4 normal, in float color, in float3 channel) {

	// Check the behavior of the material
	getMaterial ();
	float alpha = abs (material.behavior);
	color *= 1.0 - alpha;

	// The ray continues...
	
	float refractIndexFrom = 1.0;
	for (int rayBounce = 1; rayBounce < RAY_BOUNCE_MAX; ++rayBounce) {

		// Interface with the material
		float refractIndexTo;
		float3 refraction;
		if (materialTo == -1) {
			refractIndexTo = 1.0;
			refraction = refract (direction, normal.xyz, refractIndexFrom);
		} else {
			refractIndexTo = dot (material.refractIndex, channel);
			refraction = material.behavior < 0.0 ? float3 (0.0,0.0,0.0) : refract (direction, normal.xyz, refractIndexFrom / refractIndexTo);
		}
		if (dot (refraction, refraction) < DELTA) {
			direction = reflect (direction, normal.xyz);
			origin += direction * DELTA * 2.0;
		} else {
			direction = refraction;
			materialFrom = materialTo;
			refractIndexFrom = refractIndexTo;
		}

		// Ray marching
		for (int rayStep = 0; rayStep < RAY_STEP_MAX; ++rayStep) {
			float dist = max (getDistanceScene (origin), DELTA);
			normal.w += dist;
			if (materialFrom != materialTo || normal.w > RAY_LENGTH_MAX) {
				break;
			}
			origin += direction * dist;
		}

		// Check whether we hit something
		if (materialFrom == materialTo) {
			break;
		}

		// Get the normal
		if (materialTo == -1) {
			normal.xyz = -getNormal (origin, materialFrom);
		} else {
			normal.xyz = getNormal (origin, materialTo);

			// Basic lighting
			getMaterial ();
			float relfectionDiffuse = max (0.0, dot (normal.xyz, lightDirection));
			float relfectionSpecular = pow (max (0.0, dot (reflect (direction, normal.xyz), lightDirection)), SPECULAR_POWER) * SPECULAR_INTENSITY;
			float localColor = (AMBIENT + relfectionDiffuse) * CHANNEL (material.color) + relfectionSpecular;
			float localAlpha = abs (material.behavior);
			color += localColor * (1.0 - localAlpha) * alpha;
			alpha *= localAlpha;
		}
	}

	// Get the background color
	float backColor = CHANNEL (tex2D (_MainTex, direction).rgb);

	// Return the intensity of this color channel
	return color + backColor * alpha;
}



            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                
            };

            v2f vert (appdata v)
            {
                v2f o;
				//o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertex =  UnityObjectToClipPos (v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				
                return o;
            }
            
            
            
       fixed4 frag (v2f i) : SV_Target
       {
            
            fixed4 col = fixed4(0,0,0,0);
            
             //float2 fC = 
             // Define the ray corresponding to this fragment
            float2 frag = (2.0  * i.vertex.xy - _ScreenParams.xy) / _ScreenParams.y;
            
            
            float3 direction = normalize (float3 (frag, 4.0));
             
            // Set the camera
            float3 origin = 6.0 * float3 (cos (_Time.y * 0.1), 0.3 + 0.7 * sin (_Time.y * 0.2), sin (_Time.y * 0.1));
            float3 forward = -origin;
            float3 up = float3 (sin (_Time.y * 0.3), 2.0, 0.0);
            float3x3 rotation;
            rotation [2] = normalize (forward);
            rotation [0] = normalize (cross (up, forward));
            rotation [1] = cross (rotation [2], rotation [0]);
            direction = mul(rotation , direction);
            origin.y -= 1.0;
             
            // Cast the initial ray
            float4 normal = float4 (0.0,0.0,0.0,0.0);
            materialTo = -1;
            for (int rayStep = 0; rayStep < RAY_STEP_MAX; ++rayStep) {
            	float dist = max (getDistanceScene (origin), DELTA);
            	normal.w += dist;
            	if (materialTo != -1) {

            	     return fixed4(0,1,0,1);
            		break;
            	}
            	if(normal.w > RAY_LENGTH_MAX)
            	{
            	    return fixed4(0,0,1,1);
            	     return col;
            	}
            	origin += direction * dist;
            }
             
            // Check whether we hit something
            if (materialTo == -1) {
                return fixed4(1,0,0,1);
            	col.rgb = tex2D (_MainTex, direction).rgb;
            } else {
             
            	// Get the normal
            	normal.xyz = getNormal (origin, materialTo);
             
            	// Basic lighting
            	float relfectionDiffuse = max (0.0, dot (normal.xyz, lightDirection));
            	float relfectionSpecular = pow (max (0.0, dot (reflect (direction, normal.xyz), lightDirection)), SPECULAR_POWER) * SPECULAR_INTENSITY;
            	getMaterial();
            	col.rgb = (AMBIENT + relfectionDiffuse) * material.color + relfectionSpecular;
             
            	// The ray continues...
            	#ifdef PROPAGATION
            		#ifdef DISPERSION
            			col.r = raycast (origin, direction, normal, col.r, float3 (1.0, 0.0, 0.0));
            			col.g = raycast (origin, direction, normal, col.g, float3 (0.0, 1.0, 0.0));
            			col.b = raycast (origin, direction, normal, col.b, float3 (0.0, 0.0, 1.0));
            		#else
            			col.rgb = raycast (origin, direction, normal, col.rgb, float3 (1.0 / 3.0));
            		#endif
            	#endif
            }
             
            // Set the alpha channel
            col.a = 1.0;
            
            
             
            return col;

       }
            
           

			
            ENDCG
        }
    }
}
