﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Pass : MonoBehaviour
{
    public GameObject boxPrefab;
    public Queue<GameObject> tower = new Queue<GameObject>();
    public Material rMaterial;
    // Start is called before the first frame update

    public Camera camera;

    void Start()
    {
        for(int i=0;i<20;i++)
        {
            var box = Instantiate(boxPrefab, Vector3.zero + Vector3.up * i*3f, Quaternion.identity);
            tower.Enqueue(box);
        }
        //camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        var boxPoses = tower.Select(go => new Vector4(go.transform.position.x, go.transform.position.y,go.transform.position.z , 0f)).ToArray();
       
        rMaterial.SetVectorArray("_CubePA",boxPoses);
        
        rMaterial.SetVectorArray("_CubeSA", tower.Select(go => new Vector4(go.transform.localScale.x,go.transform.localScale.y, go.transform.localScale.z, 0f)).ToArray());
        var axisanglearray = tower.Select(go =>
        {
            go.transform.rotation.ToAngleAxis(out var angle, out var axis);
            return (angle, axis);
        }).ToArray();

        rMaterial.SetVectorArray("_CubeRAxis", axisanglearray.Select(axisangle=>new Vector4(axisangle.axis.x, axisangle.axis.y, axisangle.axis.z,1f)).ToArray());
        rMaterial.SetFloatArray("_CubeRAngle", axisanglearray.Select(axisangle => axisangle.angle).ToArray());
        rMaterial.SetVectorArray("_CubeEA", tower.Select(go => new Vector4(go.transform.eulerAngles.x, go.transform.eulerAngles.y, go.transform.eulerAngles.z, 0f)).ToArray());
        rMaterial.SetMatrixArray("_CubeWL", tower.Select(go => go.transform.localToWorldMatrix).ToArray());
        rMaterial.SetMatrixArray("_CubeLW", tower.Select(go => go.transform.worldToLocalMatrix).ToArray());
        rMaterial.SetVector("_CamF", new Vector4(camera.transform.forward.x, camera.transform.forward.y, camera.transform.forward.z,1f));
        rMaterial.SetVector("_CamU", new Vector4(camera.transform.up.x, camera.transform.up.y, camera.transform.up.z, 1f));
    }
}
