﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateCanvas : MonoBehaviour
{

    public RectTransform rect;
    private bool inc = true;
    // Start is called before the first frame update
    void Start()
    {
        rect = gameObject.GetComponent<RectTransform>();
        rect.anchorMin = new Vector2(0f,0f);
    }

    // Update is called once per frame
    void Update()
    {
        if(inc)
        {
            rect.anchorMin = new Vector2(rect.anchorMin.x + Time.deltaTime, rect.anchorMin.y);
            if (rect.anchorMin.x > 1f) inc = !inc;
        }
        else
        {
            rect.anchorMin = new Vector2(rect.anchorMin.x - Time.deltaTime, rect.anchorMin.y);
            if (rect.anchorMin.x < 0f) inc = !inc;
        }
        
    }
}
