﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class CameraCallback : MonoBehaviour
{
    public Material mat;
    
   
    void OnEnable()
    {
        RenderPipelineManager.beginCameraRendering += renderCamera;
    }

    void Point(float x, float y)
    {
        GL.TexCoord2(x, y);
        GL.Vertex3(x, y, -1);
    }
    void renderCamera(ScriptableRenderContext context, Camera camera)
    {
        if (camera == Camera.main)
        {
            mat.SetPass(0);
            GL.PushMatrix();
            GL.LoadIdentity();
            GL.LoadProjectionMatrix(Matrix4x4.Ortho(0,1,0,1,0,1));
            GL.Begin(GL.QUADS);
            Point(0, 0);
            Point(0, 1);
            Point(1, 1);
            Point(1, 0);
            GL.End();
            GL.PopMatrix();
        }
    }
   
    void OnDisable()
    {
        
    }
}
