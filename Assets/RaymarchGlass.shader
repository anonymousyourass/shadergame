﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/RaymarchGlass"
{
 Properties
 {
  _MainTex ("Texture", 2D) = "white" {}
        _CubeP("CubeP",Vector) = (0.1,0.1,0.1,0.1)
        _CubeS("CubeS",Vector) = (0.1,0.1,0.1,0.1)
        _CubeR("CubeR",Vector) = (0.1,0.1,0.1,0.1)
        

        _Speed("Speed",Range(0,100)) = 50
        _Shift("Shift",Range(0,100)) = 0
 }
 SubShader
 {
  Tags { "RenderType"="Opaque" }
  LOD 100

  Pass
  {
   CGPROGRAM
   #pragma vertex vert
   #pragma fragment frag
   
   #include "UnityCG.cginc"



#define DELTA    0.001
#define RAY_COUNT   7
#define RAY_LENGTH_MAX  100.0
#define RAY_STEP_MAX  100
#define LIGHT    fixed3 (1.0, 1.0, -1.0)
#define REFRACT_FACTOR  0.6
#define REFRACT_INDEX  1.6
#define AMBIENT    0.2
#define SPECULAR_POWER  3.0
#define SPECULAR_INTENSITY 0.5
#define FADE_POWER   1.0
#define M_PI    3.1415926535897932384626433832795
#define GLOW_FACTOR   1.5
#define LUMINOSITY_FACTOR 2.0
#define DEG2RAD  0.017453292

   struct appdata
   {
    float4 vertex : POSITION;
    float2 uv : TEXCOORD0;
   };

   struct v2f
   {
    float2 uv : TEXCOORD0;
    float4 vertex : SV_POSITION;
    float3 ro : TEXCOORD1;
    float3 hitPos : TEXCOORD2;
   };

   sampler2D _MainTex;
   float4 _MainTex_ST;

   fixed4 _CubeP;
   fixed4 _CubeS;
   fixed4 _CubeR;

            fixed _Speed;
            uniform fixed4 _CubePA[20];
            uniform fixed4 _CubeSA[20];
            uniform fixed4 _CubeRAxis[20];
            uniform fixed _CubeRAngle[20];
            uniform fixed4 _CubeEA[20];

            uniform fixed4x4 _CubeWL[20];
            uniform fixed4x4 _CubeLW[20];

            uniform fixed4 _CamF;
            uniform fixed4 _CamU;

                fixed _Shift;
   v2f vert (appdata v)
   {
    v2f o;
    o.vertex = UnityObjectToClipPos(v.vertex);
    o.uv = TRANSFORM_TEX(v.uv, _MainTex);

    o.ro = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos,1));
    o.hitPos = v.vertex;
    
    return o;
   }

fixed3x3 mRotate (in fixed3 angle) {
 float c = cos (angle.x);
 float s = sin (angle.x);
 fixed3x3 rx = fixed3x3 (1.0, 0.0, 0.0, 0.0, c, s, 0.0, -s, c);

 c = cos (angle.y);
 s = sin (angle.y);
 fixed3x3 ry = fixed3x3 (c, 0.0, -s, 0.0, 1.0, 0.0, s, 0.0, c);

 c = cos (angle.z);
 s = sin (angle.z);
 fixed3x3 rz = fixed3x3 (c, s, 0.0, -s, c, 0.0, 0.0, 0.0, 1.0);

 return mul(mul(rz,ry), rx);
}






static fixed3 k = fixed3(0,0,0);

float sdBox( fixed3 p, fixed3 b )
{
  fixed3 q = abs(p) - b;
  return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
}


fixed4x4 inverse( in fixed4x4 m )
{
 return fixed4x4(//todo columns is rows
        m[0][0], m[1][0], m[2][0],0 ,
        m[0][1], m[1][1], m[2][1], 0,
        m[0][2], m[1][2], m[2][2], 0,
        -dot(m[0].xyz,m[3].xyz),
        -dot(m[1].xyz,m[3].xyz),
        -dot(m[2].xyz,m[3].xyz),
        1.0 );
}

fixed4x4 inversep( in fixed4x4 m )
{
 return fixed4x4(
        m[0][0], m[0][1], m[0][2], -dot(m[0].xyz,m[3].xyz) ,
        m[1][0], m[1][1], m[1][2], -dot(m[1].xyz,m[3].xyz),
        m[2][0], m[2][1], m[2][2], -dot(m[2].xyz,m[3].xyz),
        0,
        0,
        0,
        1.0 );
}



fixed4x4 rotationAxisAngle( fixed3 v, float angle )
{
    float s = sin( angle );
    float c = cos( angle );
    float ic = 1.0 - c;

    return fixed4x4( v.x*v.x*ic + c,     v.y*v.x*ic - s*v.z, v.z*v.x*ic + s*v.y, 0.0,
                 v.x*v.y*ic + s*v.z, v.y*v.y*ic + c,     v.z*v.y*ic - s*v.x, 0.0,
                 v.x*v.z*ic - s*v.y, v.y*v.z*ic + s*v.x, v.z*v.z*ic + c,     0.0,
        0.0,                0.0,                0.0,                1.0 );
}

float4x4 m_scale(float4x4 m, float3 v)
{
    float x = v.x, y = v.y, z = v.z;

    m[0][0] *= x; m[1][0] *= y; m[2][0] *= z;
    m[0][1] *= x; m[1][1] *= y; m[2][1] *= z;
    m[0][2] *= x; m[1][2] *= y; m[2][2] *= z;
    m[0][3] *= x; m[1][3] *= y; m[2][3] *= z;

    return m;
}

fixed4x4 translate( fixed x, fixed y, fixed z )
{
    return fixed4x4( 1.0, 0.0, 0.0, 0,
                     0.0, 1.0, 0.0, 0,
                     0.0, 0.0, 1.0, 0,
                     x,   y,   z,   1.0 );
}

fixed4x4 translatep( fixed x, fixed y, fixed z )
{
    return fixed4x4( 1.0, 0.0, 0.0, x,
                     0.0, 1.0, 0.0, y,
                     0.0, 0.0, 1.0, z,
                     0,   0,   0,   1.0 );
}

float getDistance (fixed3 p,in fixed3 rd) {
    fixed box1 = 100;
    for (int i = 0; i < 20; i++)
    {
        fixed3 po = p;
        fixed3 b = 0.5 * fixed3(1,1,1);

        //fixed xradians = _CubeEA[i].x * DEG2RAD;
        //fixed yradians = _CubeEA[i].y * DEG2RAD;
        //fixed zradians = _CubeEA[i].z * DEG2RAD;
        //
        //fixed4x4 rotx = rotationAxisAngle( normalize(fixed3(1.0,0,0.0)),xradians);
        //fixed4x4 roty = rotationAxisAngle( normalize(fixed3(0,1.0,0.0)),yradians);
        //fixed4x4 rotz = rotationAxisAngle( normalize(fixed3(0,0,1.0)),zradians);

        //fixed4x4 rot = mul(mul(rotz,roty),rotx);
        fixed4x4 rot = _CubeLW[i];
        
        fixed4x4 tra = translate(_CubePA[i].x,_CubePA[i].y,_CubePA[i].z);
        tra = m_scale(tra,float3(1/_CubeSA[i].x,1/_CubeSA[i].y,1/_CubeSA[i].z));
        fixed4x4 txi = mul(tra,rot);
           

        //fixed4x4 txx = inverse( txi );

        po = mul(fixed4(po.x,po.y,po.z,1.0),txi);
       

        
        fixed box2 = sdBox(po,b);

        box1 = min(box1,box2);
    }
    
    return box1;


 

}

fixed3 getFragmentColor (in fixed3 origin, in fixed3 direction) {
 fixed3 lightDirection = normalize (LIGHT);
 fixed2 delta = fixed2 (DELTA, 0.0);

 fixed3 fragColor = fixed3 (0.0, 0.0, 0.0);
 float intensity = 1.0;

 float distanceFactor = 1.0;
 float refractionRatio = 1.0 / REFRACT_INDEX;
 float rayStepCount = 0.0;
 for (int rayIndex = 0; rayIndex < RAY_COUNT; ++rayIndex) {

  // Ray marching
  float dist = RAY_LENGTH_MAX;
  float rayLength = 0.0;
  for (int rayStep = 0; rayStep < RAY_STEP_MAX; ++rayStep) {
   dist = distanceFactor * getDistance (origin,direction);
   float distMin = max (dist, DELTA);
   rayLength += distMin;
   if (dist < 0.0 || rayLength > RAY_LENGTH_MAX) {
    break;
   }
   origin += direction * distMin;
   ++rayStepCount;
  }

  // Check whether we hit something
  fixed3 backColor = fixed3 (0.0, 0.0, 0.1 + 0.2 * max (0.0, dot (-direction, lightDirection)));
  if (dist >= 0.0) {
   fragColor = fragColor * (1.0 - intensity) + backColor * intensity;
   break;
  }

  // Get the normal
  fixed3 normal = normalize (distanceFactor * fixed3 (
   getDistance (origin + delta.xyy,direction) - getDistance (origin - delta.xyy,direction),
   getDistance (origin + delta.yxy,direction) - getDistance (origin - delta.yxy,direction),
   getDistance (origin + delta.yyx,direction) - getDistance (origin - delta.yyx,direction)));

  // Basic lighting
  fixed3 reflection = reflect (direction, normal);
  if (distanceFactor > 0.0) {
   float relfectionDiffuse = max (0.0, dot (normal, lightDirection));
   float relfectionSpecular = pow (max (0.0, dot (reflection, lightDirection)), SPECULAR_POWER) * SPECULAR_INTENSITY;
   float fade = pow (1.0 - rayLength / RAY_LENGTH_MAX, FADE_POWER);

   fixed3 localColor = max (sin (k * k), 0.2);
   localColor = (AMBIENT + relfectionDiffuse) * localColor + relfectionSpecular;
   localColor = lerp (backColor, localColor, fade);

   fragColor = fragColor * (1.0 - intensity) + localColor * intensity;
   intensity *= REFRACT_FACTOR;
  }

  // Next ray...
  fixed3 refraction = refract (direction, normal, refractionRatio);
  if (dot (refraction, refraction) < DELTA) {
   direction = reflection;
   origin += direction * DELTA * 2.0;
  }
  else {
   direction = refraction;
   distanceFactor = -distanceFactor;
   refractionRatio = 1.0 / refractionRatio;
  }
 }

 // Return the fragment color
 return fragColor * LUMINOSITY_FACTOR + GLOW_FACTOR * rayStepCount / float (RAY_STEP_MAX * RAY_COUNT);
}



   fixed4 frag (v2f i) : SV_Target
   {

     //discard;
                
                // Define the ray corresponding to this fragment
             fixed2 frag = (2.0 * i.vertex.xy - _ScreenParams.xy) / _ScreenParams.y;



             fixed3 direction = normalize (fixed3 (frag.x,frag.y, 2.0));

             // Set the camera
             //fixed3 origin = fixed3 ((15.0 * cos (_Time.y * 0.1*_Speed)), 10.0 * sin (_Time.y * 0.2*_Speed), 15.0 * sin (_Time.y * 0.1*_Speed));
                //fixed3 origin = fixed3 (0.0,0.0,-20.0);

                fixed3 origin = _WorldSpaceCameraPos;

                

             //fixed3 forward = -origin;
                //fixed3 forward = fixed3(0.0,0.0,1.0);
                fixed3 forward = fixed3(_CamF.x,_CamF.y,_CamF.z);

             //fixed3 up = fixed3 (sin (_Time.y * 0.3*_Speed), 2.0, 0.0);
                fixed3 up = fixed3 (_CamU.x,_CamU.y,_CamU.z);

             fixed3x3 rotation;
             rotation [2] = normalize (forward);//_CamF.xyz
             rotation [0] = normalize (cross (up, forward));
             rotation [1] = cross (rotation [2], rotation [0]);
             

             origin+= direction*(unity_OrthoParams.y+unity_OrthoParams.y);//rotation [1] * i.uv.x*_CubeP.x + up * i.uv.y*_CubeP.x

//direction = mul(rotation ,direction);
             direction =  forward ;

               
             // Set the fragment color
             fixed4 col = fixed4 (getFragmentColor (origin, direction), 1.0);


    return col;
   }
   ENDCG
  }
 }
}