
Shader "ShaderMan/Minkovsky"
	{

	Properties{
	//Properties
	}

	SubShader
	{
	Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }

	Pass
	{
	ZWrite Off
	Blend SrcAlpha OneMinusSrcAlpha

	CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
	#include "UnityCG.cginc"

	struct VertexInput {
    fixed4 vertex : POSITION;
	fixed2 uv:TEXCOORD0;
    fixed4 tangent : TANGENT;
    fixed3 normal : NORMAL;
	//VertexInput
	};


	struct VertexOutput {
	fixed4 pos : SV_POSITION;
	fixed2 uv:TEXCOORD0;
	//VertexOutput
	};

	//Variables

	

// 2D rotation. Always handy.
fixed2x2 rot(fixed th){ fixed cs = cos(th), si = sin(th); return fixed2x2(cs, -si, si, cs); }


fixed Voronesque( in fixed3 p ){
    
    // Skewing the cubic grid, then determining the first vertice.
    fixed3 i  = floor(p + dot(p, fixed3(0.333333,0.333333,0.333333)) );  p -= i - dot(i, fixed3(0.166666,0.166666,0.166666)) ;
    
    // Breaking the skewed cube into tetrahedra with partitioning planes, then determining which side of the 
    // intersecting planes the skewed point is on. Ie: Determining which tetrahedron the point is in.
    fixed3 i1 = step(0., p-p.yzx), i2 = max(i1, 1.0-i1.zxy); i1 = min(i1, 1.0-i1.zxy);    
    
    // Using the above to calculate the other three vertices. Now we have all four tetrahedral vertices.
    fixed3 p1 = p - i1 + 0.166666, p2 = p - i2 + 0.333333, p3 = p - 0.5;
    
    fixed3 rnd = fixed3(7, 157, 113); // I use this combination to pay homage to Shadertoy.com. :)
    
    // Falloff values from the skewed point to each of the tetrahedral points.
    fixed4 v = max(0.5 - fixed4(dot(p, p), dot(p1, p1), dot(p2, p2), dot(p3, p3)), 0.);
    
    // Assigning four random values to each of the points above. 
    fixed4 d = fixed4( dot(i, rnd), dot(i + i1, rnd), dot(i + i2, rnd), dot(i + 1., rnd) ); 
    
    // Further randomizing "d," then combining it with "v" to produce the final random falloff values. Range [0, 1]
    d = frac(sin(d)*262144.)*v*2.; 
 
    // Reusing "v" to determine the largest, and second largest falloff values. Analogous to distance.
    v.x = max(d.x, d.y), v.y = max(d.z, d.w), v.z = max(min(d.x, d.y), min(d.z, d.w)), v.w = min(v.x, v.y); 
   
    return  max(v.x, v.y)- max(v.z, v.w); // Maximum minus second order, for that beveled Voronoi look. Range [0, 1].
    //return max(v.x, v.y); // Maximum, or regular value for the regular Voronoi aesthetic.  Range [0, 1].
}






	VertexOutput vert (VertexInput v)
	{
	VertexOutput o;
	o.pos = UnityObjectToClipPos (v.vertex);
	o.uv = v.uv;
	//VertexFactory
	return o;
	}
	fixed4 frag(VertexOutput i) : SV_Target
	{
	
    
    // Screen coordinates, plus some movement about the center.
    fixed2 uv = (i.uv - 1*0.5)/1 + fixed2(0.5*cos(_Time.y*0.5), 0.25*sin(_Time.y*0.5));
    
    // Unit direction ray.
    fixed3 rd = normalize(fixed3(uv, 1.));
    rd.xy = mul(rd.xy, rot(sin(_Time.y*0.25)*0.5)); // Very subtle look around, just to show it's a 3D effect.
    rd.xz = mul(rd.xz,rot(sin(_Time.y*0.25)*0.5));
 
    // Screen color. Initialized to black.
    fixed3 col = fixed3(0,0,0);
    
    // Ray intersection of a cylinder (radius one) - centered at the origin - from a ray-origin that has XY coordinates 
    // also centered at the origin.
    //fixed sDist = max(dot(rd.xy, rd.xy), 1e-16); // Analogous to the surface function.
    //sDist = 1./sqrt(sDist); // Ray origin to surface distance.
    
    // Same as above, but using a Minkowski distance and scaling factor. I tried it on a whim, and it seemed to work.
    // I know, not scientific at all, but it kind of makes sense. They'll let anyone get behind a computer these days. :)
    fixed2 scale = fixed2(0.75, 1.);
    fixed power = 2.;
    fixed sDist = max(dot( pow(abs(rd.xy)*scale, fixed2(power,power)), fixed2(1.,1.) ), 1e-16); // Analogous to the surface function.
    sDist = 1./pow( sDist, 1./power ); // Ray origin to surface distance.
    
    //if(sDist>1e-8){
        
        // Using the the distance "sDist" above to calculate the surface position. Ie: sp = ro + rd*t;
        // I've hardcoded "ro" to reduce line count. Note that "ro.xy" is centered on zero. The cheap
        // ray-intersection formula above relies on that.
        fixed3 sp = fixed3(0.0, 0.0, _Time.y*2.) + rd*sDist;
 
        // The surface normal. Based on the derivative of the surface description function. See above.
        //fixed3 sn = normalize(fixed3(-sp.xy, 0.)); // Cylinder normal.
        fixed3 sn = normalize(-sign(sp)*fixed3(pow(abs(sp.xy)*scale, fixed2(power-1.,power-1.)), 0.)); // Minkowski normal.
        
        // Bump mapping.
        //
        // I wanted to make this example as simple as possible, but it's only a few extra lines. Note the larger 
        // "eps" number. Increasing the value spreads the samples out, which effectively blurs the result, thus 
        // reducing the jaggies. The downside is loss of bump precision, which isn't noticeable in this particular 
        // example. Decrease the value to "0.001" to see what I'm talking about.
        const fixed2 eps = fixed2(0.025, 0.);
        fixed c = Voronesque(sp*2.5); // Base value. Used below to color the surface.
        // 3D gradient fixedtor... of sorts. Based on the bump function. In this case, Voronoi.                
        fixed3 gr = (fixed3(Voronesque((sp-eps.xyy)*2.5), Voronesque((sp-eps.yxy)*2.5), Voronesque((sp-eps.yyx)*2.5))-c)/eps.x;
        gr -= sn*dot(sn, gr); // There's a reason for this... but I need more room. :)
        sn = normalize(sn + gr*0.1); // Combining the bump gradient fixedtor with the object surface normal.

        // Lighting.
        //
        // The light is hovering just in front of the viewer.
        fixed3 lp = fixed3(0.0, 0.0, _Time.y*2. + 3.);
        fixed3 ld = lp - sp; // Light direction.
        fixed dist = max(length(ld), 0.001); // Distance from light to the surface.
        ld /= dist; // Use the distance to normalize "ld."

        // Light attenuation, based on the distance above.
        fixed atten = min(1.0/max(0.75 + dist*0.25 + dist*dist*0.05, 0.001), 1.0);
        
       
        fixed diff = max(dot(sn, ld), 0.); // Diffuse light value.
        fixed spec = pow(max(dot(reflect(-ld, sn), -rd), 0.), 16.); // Specular highlighting.
        // Adding some fake, reflective environment information.
        fixed ref = Voronesque((sp + reflect(rd, sn)*0.5)*2.5);
        
        // Coloring the surface with the Voronesque function that is used to bump the surface. See "bump mapping" above.
        fixed3 objCol = fixed3(min(c*1.5, 1.), pow(c, 2.5), pow(c, 12.)); // Cheap, but effective, red palette.
        //fixed3 objCol = fixed3(c*c*0.9, c, c*c*0.4); // Cheap green palette.
        //fixed3 objCol = fixed3(pow(c, 1.6), pow(c, 1.7), c); // Purpley blue.
        //fixed3 objCol = fixed3(c,c,c); // Grey scale.

        // Using the values above to produce the final color.
        //col = (objCol*(diff + ref*0.25 + 0.25) + fixed3(1., 0.8, 0.9)*ref*0.25 + spec*fixed3(0.75, 0.9, 1.))*atten;
        col = (objCol*(fixed3(1.0, 0.97, 0.92)*diff + ref*0.5 + 0.25) + fixed3(1., 0.8, 0.9)*ref*0.3 + fixed3(1., 0.9, 0.7)*spec)*atten;
        //col = ((fixed3(1.0, 0.97, 0.92)*diff + ref*0.5 + 0.25)*c + fixed3(1., 0.8, 0.9)*ref*0.3 + fixed3(0.75, 0.9, 1.)*spec)*atten;
         
       return float4(min(col, 1.), 1.);
    //
	}
	ENDCG
	}
  }
}

