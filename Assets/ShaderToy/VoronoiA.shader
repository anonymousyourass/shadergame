
Shader "ShaderMan/VoronoiA"
	{

	Properties{
	_MainTex ("MainTex", 2D) = "white" {}
	}

	SubShader
	{
	Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }

	Pass
	{
	ZWrite Off
	Blend SrcAlpha OneMinusSrcAlpha

	CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
	#include "UnityCG.cginc"

	struct VertexInput {
    fixed4 vertex : POSITION;
	fixed2 uv:TEXCOORD0;
    fixed4 tangent : TANGENT;
    fixed3 normal : NORMAL;
	//VertexInput
	};


	struct VertexOutput {
	fixed4 pos : SV_POSITION;
	fixed2 uv:TEXCOORD0;
	//VertexOutput
	};

	//Variables
sampler2D _MainTex;

	
#define ANIMATE

fixed2 hash2( fixed2 p )
{
	// tex2D based white noise
	return tex2Dlod( _MainTex,float4( (p+0.5)/256.0, 0.0 ,0)).xy;
	
    // procedural white noise	
	//return frac(sin(fixed2(dot(p,fixed2(127.1,311.7)),dot(p,fixed2(269.5,183.3))))*43758.5453);
}

fixed3 voronoi( in fixed2 x )
{
    fixed2 n = floor(x);
    fixed2 f = frac(x);

    //----------------------------------
    // first pass: regular voronoi
    //----------------------------------
	fixed2 mg, mr;

    fixed md = 8.0;
    [unroll(100)]
for( int j=-1; j<=1; j++ )
    [unroll(100)]
for( int i=-1; i<=1; i++ )
    {
        fixed2 g = fixed2(fixed(i),fixed(j));
		fixed2 o = hash2( n + g );
		#ifdef ANIMATE
        o = 0.5 + 0.5*sin( _Time.y + 6.2831*o );
        #endif	
        fixed2 r = g + o - f;
        fixed d = dot(r,r);

        if( d<md )
        {
            md = d;
            mr = r;
            mg = g;
        }
    }

    //----------------------------------
    // second pass: distance to borders
    //----------------------------------
    md = 8.0;
    [unroll(100)]
for( int j=-2; j<=2; j++ )
    [unroll(100)]
for( int i=-2; i<=2; i++ )
    {
        fixed2 g = mg + fixed2(fixed(i),fixed(j));
		fixed2 o = hash2( n + g );
		#ifdef ANIMATE
        o = 0.5 + 0.5*sin( _Time.y + 6.2831*o );
        #endif	
        fixed2 r = g + o - f;

        if( dot(mr-r,mr-r)>0.00001 )
        md = min( md, dot( 0.5*(mr+r), normalize(r-mr) ) );
    }

    return fixed3( md, mr );
}





	VertexOutput vert (VertexInput v)
	{
	VertexOutput o;
	o.pos = UnityObjectToClipPos (v.vertex);
	o.uv = v.uv;
	//VertexFactory
	return o;
	}
	fixed4 frag(VertexOutput i) : SV_Target
	{
	
    fixed2 p = i.uv/1;

    fixed3 c = voronoi( 8.0*p );

	// isolines
    fixed3 col = c.x*(0.5 + 0.5*sin(64.0*c.x))*fixed3(1.0,1.0,1.0);
    // borders	
    col = lerp( fixed3(1.0,0.6,0.0), col, smoothstep( 0.04, 0.07, c.x ) );
    // feature points
	fixed dd = length( c.yz );
	col = lerp( fixed3(1.0,0.6,0.1), col, smoothstep( 0.0, 0.12, dd) );
	col += fixed3(1.0,0.6,0.1)*(1.0-smoothstep( 0.0, 0.04, dd));

	return fixed4(col,1.0);

	}
	ENDCG
	}
  }
}

